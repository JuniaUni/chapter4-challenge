const express = require('express');
const router = express.Router();
const c = require('../controllers');
const mid = require('../helpers/middleware');

// auth
router.post('/auth/register', c.auth.register);
router.post('/auth/login', c.auth.login);

// biodata
router.get('/biodata/index', mid.mustLogin, c.biodata.index);
router.post('/biodata/create', mid.mustLogin, c.biodata.create);
router.put('/biodata/update/:id', mid.mustLogin, c.biodata.update);
router.delete('/biodata/delete/:id', mid.mustLogin, c.biodata.delete);

// data history
router.get('/history/index', mid.mustLogin, c.history.index);
router.post('/history/create', mid.mustLogin, c.history.create);
router.put('/history/update/:id', mid.mustLogin, c.history.update);
router.delete('/history/delete/:id', mid.mustLogin, c.history.delete);


module.exports = router;