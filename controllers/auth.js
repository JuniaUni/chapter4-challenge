const { User } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {
    JWT_SIGNATURE_KEY
} = process.env

module.exports = {
    register: async (req, res, next) => {
        try {
            const {username, password} = req.body;

            const existUser = await User.findOne({where: {username: username}});
            if (existUser) {
                return res.status(409).json({
                    status: false,
                    message: 'username already use!'
                });
            }

            const encryptedPassword = await bcrypt.hash(password, 10);

            const user = await User.create({
                username,
                password: encryptedPassword
            });

            return res.status(201).json({
                status: true,
                message: 'success',
                data: {
                    username: user.username
                }
            });
            
        } catch (err) {
            next(err);
        }
    },

    login: async (req, res, next) => {
        try {
            const {username, password} = req.body;

            const user = await User.findOne({where: {username: username}});
            if (!user) {
                return res.status(404).json({
                    status: false,
                    message: 'username or password doesn\'t match!'
                });
            }

            const pwdCorrect = await bcrypt.compare(password, user.password);

            if (!pwdCorrect) {
                return res.status(400).json({
                    status: false,
                    message: 'username or password doesn\'t match!'
                });
            }
            
            // generate
            payload = {
                id: user.id,
                username: user.username
            }
            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    token: token
                }
            });
            
        } catch (err) {
            next(err);
        }
    }
};