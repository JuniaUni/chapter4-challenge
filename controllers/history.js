const History = require('../models').History;

module.exports = {
  index: (req, res) => {
    return History
      .findAll({
        include: [],
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((history) => res.status(200).send(history))
      .catch((err) => { res.status(400).send(err); });
  },

  create: (req, res) => {
    return History
    .create({
        score: req.body.score
    })
    .then((history) => res.status(201).send(history))
    .catch((err) => res.status(400).send(err));
  },

  update: (req, res) => {
    return History
      .findByPk(req.params.id)      
      .then(history => {
        if (!history) {
          return res.status(404).send({
            message: 'history Not Found',
          });
        }
        return history
          .update({
            score: req.body.score || history.score
          })
          .then(() => res.status(200).send(history))
          .catch((err) => res.status(400).send(err));
      })
      .catch((err) => res.status(400).send(err));
  },

  delete: (req, res) => {
    return History
      .findByPk(req.params.id)
      .then(history => {
        if (!history) {
          return res.status(400).send({
            message: 'History Not Found',
          });
        }
        return history
          .destroy()
          .then(() => res.status(204).send())
          .catch((err) => res.status(400).send(err));
      })
      .catch((err) => res.status(400).send(err));
    }
};