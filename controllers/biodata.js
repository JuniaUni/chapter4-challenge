const Biodata = require('../models').Biodata;

module.exports = {
  index: (req, res) => {
    return Biodata
      .findAll({
        include: [],
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((biodata) => res.status(200).send(biodata))
      .catch((err) => { res.status(400).send(err); });
  },

  create: (req, res) => {
    return Biodata
    .create({
        name: req.body.name,
        gender: req.body.gender,
        age: req.body.age
    })
    .then((biodata) => res.status(201).send(biodata))
    .catch((err) => res.status(400).send(err));
  },

  update: (req, res) => {
    return Biodata
      .findByPk(req.params.id)      
      .then(biodata => {
        if (!biodata) {
          return res.status(404).send({
            message: 'biodata Not Found',
          });
        }
        return biodata
          .update({
            name: req.body.name || biodata.name,
            gender: req.body.gender || biodata.gender,
            age: req.body.age || biodata.age
          })
          .then(() => res.status(200).send(biodata))
          .catch((err) => res.status(400).send(err));
      })
      .catch((err) => res.status(400).send(err));
  },

  delete: (req, res) => {
    return Biodata
      .findByPk(req.params.id)
      .then(biodata => {
        if (!biodata) {
          return res.status(400).send({
            message: 'Biodata Not Found',
          });
        }
        return biodata
          .destroy()
          .then(() => res.status(204).send())
          .catch((err) => res.status(400).send(err));
      })
      .catch((err) => res.status(400).send(err));
    }
};